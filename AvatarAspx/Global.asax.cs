﻿using System.Web.Mvc;
using System.Web.Routing;

namespace AvatarAspx
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute("Avatar", "avatar", new { controller = "Avatar", action = "Index" });
            routes.MapRoute("Avatar_Hash", "avatar/{emailHash}", new { controller = "Avatar", action = "Get" });

            routes.MapRoute("Site", "", new { controller = "Site", action = "Index" });
            routes.MapRoute("Site_Signup", "site/signup", new { controller = "Site", action = "SignUp" });
            routes.MapRoute("Site_Logout", "site/logout", new { controller = "Site", action = "Logout" });

            routes.MapRoute("Emails", "emails", new { controller = "Emails", action = "Index" });
            routes.MapRoute("Emails_New", "emails/new", new { controller = "Emails", action = "New" });
            routes.MapRoute("Emails_Add", "emails/add", new { controller = "Emails", action = "Add" });
            routes.MapRoute("Emails_ConfirmMakePrimary", "emails/confirm-make-primary", new { controller = "Emails", action = "ConfirmMakePrimary" });
            routes.MapRoute("Emails_AssociateImage", "emails/associate-image", new { controller = "Emails", action = "AssociateImage" });
            routes.MapRoute("Emails_RemoveUserImage", "emails/remove-userimage/{userHash}", new { controller = "Emails", action = "RemoveUserImage" });

            routes.MapRoute("Heads_New", "heads/new", new { controller = "Heads", action = "New" });
            routes.MapRoute("Heads_New_Computer", "heads/new/computer", new { controller = "Heads", action = "NewComputer" });
            routes.MapRoute("Heads_New_URL", "heads/new/url", new { controller = "Heads", action = "NewURL" });
            routes.MapRoute("Heads_New_Existing", "heads/new/existing", new { controller = "Heads", action = "NewExisting" });
            routes.MapRoute("Heads_Fetch", "heads/fetch", new { controller = "Heads", action = "Fetch" });
            routes.MapRoute("Heads_Upload", "heads/upload", new { controller = "Heads", action = "Upload" });
            routes.MapRoute("Heads_DeleteUserImage", "heads/delete-userimage/{imageHash}", new { controller = "Heads", action = "DeleteUserImage" });

            routes.MapRoute("UserImage", "userimage/{emailHash}/{imageFile}", new { controller = "UserImage", action = "Get" });
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
        }
    }
}