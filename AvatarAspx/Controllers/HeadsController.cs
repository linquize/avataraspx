﻿using System;
using System.IO;
using System.Web;
using System.Web.Mvc;

namespace AvatarAspx.Controllers
{
    public class HeadsController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult New()
        {
            return View();
        }

        public ActionResult NewComputer()
        {
            return View();
        }

        public ActionResult NewURL()
        {
            return View();
        }

        public ActionResult NewExisting()
        {
            return View();
        }

        public ActionResult Fetch()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase head_file)
        {
            string emailHash = (string)Session["emailHash"];
            string tempFile = Path.GetTempFileName();
            head_file.SaveAs(tempFile);
            string imageHash;
            using (var stream = System.IO.File.OpenRead(tempFile))
                imageHash = CalcMD5(stream);
            string imageName = imageHash + Path.GetExtension(head_file.FileName);
            string filename = Path.Combine(BasePath, emailHash, imageName);
            System.IO.File.Copy(tempFile, filename, true);
            System.IO.File.AppendAllText(Path.Combine(BasePath, emailHash, "files.txt"), imageName + Environment.NewLine);
            System.IO.File.WriteAllText(Path.Combine(BasePath, emailHash, "current.txt"), imageName);
            System.IO.File.Delete(tempFile);
            return RedirectToAction("Index", "Emails");
        }
    }
}
