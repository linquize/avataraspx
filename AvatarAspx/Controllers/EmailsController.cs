﻿using System.IO;
using System.Linq;
using System.Web.Mvc;
using AvatarAspx.Models;

namespace AvatarAspx.Controllers
{
    public class EmailsController : BaseController
    {
        public ActionResult Index()
        {
            string emailHash = (string)Session["emailHash"];
            string dir = Path.Combine(BasePath, emailHash);
            string[] files = { };
            if (System.IO.File.Exists(Path.Combine(BasePath, emailHash, "files.txt")))
                files = System.IO.File.ReadAllLines(Path.Combine(BasePath, emailHash, "files.txt"));
            return View(new EmailsIndexModel
            {
                Images = files.Where(a => a.Length > 0)
                    .Select(a => new EmailsIndexModel.ImageEntry
                    {
                        Hash = Path.GetFileNameWithoutExtension(a),
                        Title = "",
                        Url = this.Url.Action("Get", "UserImage", new { emailHash = emailHash, imageFile = Path.GetFileName(a) })
                    })
            });
        }

        public ActionResult New()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(string email)
        {
            return View();
        }
    }
}
