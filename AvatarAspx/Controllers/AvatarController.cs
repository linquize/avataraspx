﻿using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace AvatarAspx.Controllers
{
    public class AvatarController : BaseController
    {
        public ActionResult Index()
        {
            return this.File("~/Content/AvatarAspx.png", "image/png");
        }

        public ActionResult Get(string emailHash)
        {
            try
            {
                string current = System.IO.File.ReadAllText(Path.Combine(BasePath, emailHash, "current.txt"));
                string file = Path.Combine(BasePath, emailHash, current);
                return this.File(file, GetMime(current));
            }
            catch
            {
                return Fallback(emailHash);
            }
        }

        public ActionResult Fallback(string emailHash)
        {
            try
            {
                string url = "http://www.gravatar.com/avatar/" + emailHash + Request.Url.Query;
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                AddHeader("Cache-Control", response);
                AddHeader("Expires", response);
                AddHeader("Last-Modified", response);
                return this.File(response.GetResponseStream(), response.ContentType);
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    HttpWebResponse response = (HttpWebResponse)ex.Response;
                    Response.StatusCode = (int)response.StatusCode;
                    AddHeader("Cache-Control", response);
                    AddHeader("Expires", response);
                    AddHeader("Last-Modified", response);
                    return this.File(response.GetResponseStream(), response.ContentType);
                }
                return Index();
            }
            catch
            {
                return Index();
            }
        }

        private void AddHeader(string key, HttpWebResponse response)
        {
            string value = response.Headers[key];
            if (!string.IsNullOrEmpty(value))
                this.Response.AddHeader(key, value);
        }
    }
}
