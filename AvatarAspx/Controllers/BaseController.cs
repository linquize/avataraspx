﻿using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web.Mvc;

namespace AvatarAspx.Controllers
{
    public class BaseController : Controller
    {
        public string BasePath = Path.Combine(Path.GetTempPath(), "AvatarAspx");

        public static string CalcMD5(string email)
        {
            using (var csp = new MD5CryptoServiceProvider())
            {
                byte[] buf = Encoding.UTF8.GetBytes(email);
                byte[] result = csp.ComputeHash(buf);
                return string.Concat(result.Select(a => a.ToString("x2")));
            }
        }

        public static string CalcMD5(Stream stream)
        {
            using (var csp = new MD5CryptoServiceProvider())
            {
                byte[] buf = new byte[65536];
                int read = 0;
                while ((read = stream.Read(buf, 0, buf.Length)) > 0)
                    csp.TransformBlock(buf, 0, read, buf, 0);
                csp.TransformFinalBlock(buf, 0, 0);
                return string.Concat(csp.Hash.Select(a => a.ToString("x2")));
            }
        }

        public static string GetMime(string filename)
        {
            switch (Path.GetExtension(filename))
            {
                case ".png":
                    return "image/png";
                case ".jpg":
                    return "image/jpg";
                case ".gif":
                    return "image/gif";
            }
            return "image/png";
        }
    }
}
