﻿using System.IO;
using System.Web.Mvc;

namespace AvatarAspx.Controllers
{
    public class SiteController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SignUp(string email)
        {
            string emailHash = CalcMD5(email);
            Directory.CreateDirectory(Path.Combine(BasePath, emailHash));
            System.IO.File.WriteAllText(Path.Combine(BasePath, emailHash, "email.txt"), email);
            Session["emailHash"] = emailHash;
            return View();
        }

        public ActionResult Logout()
        {
            Session.Abandon();
            return View();
        }
    }
}
