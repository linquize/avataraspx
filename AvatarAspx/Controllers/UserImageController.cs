﻿using System.IO;
using System.Web.Mvc;

namespace AvatarAspx.Controllers
{
    public class UserImageController : BaseController
    {
        public ActionResult Get(string emailHash, string imageFile)
        {
            return this.File(Path.Combine(BasePath, emailHash, imageFile), GetMime(imageFile));
        }
    }
}
