﻿using System.Collections.Generic;

namespace AvatarAspx.Models
{
    public class EmailsIndexModel
    {
        public class ImageEntry
        {
            public string Hash { get; set; }
            public string Url { get; set; }
            public string Title { get; set; }
        }

        public IEnumerable<ImageEntry> Images { get; set; }
    }
}